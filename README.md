# About

This project aim to increase developer productivity.
CI pipeline make it easy to create docker image based on dockerfile.
Terraform files create aws fargate infrastructure.

## Requirements
1- You need a shared_credentials_file in "$HOME/.aws/credentials". You can check to documentation \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://docs.aws.amazon.com/ses/latest/dg/create-shared-credentials-file.html \
2- "terraform" binary. Version must be higher than 0.12
3- "npm" binary for local wep development

## What will be automated in the future(Infrastructure)
Current configuration supports only aws provider. If application will be run on multi cloud environment other cloud provider terraform script can be added.
Terraform state stored in local development machine. Terraform state must be stored on cloud or internal artifactory tools. 


## What will be automated in the future(CI/CD)
Multi branch pipeline is not supported.
For production usage docker images must be created for test,uat and prod environments.
Pipeline must be splited to stages. Currently just one stage is used for build.
Test,build,deploy,notification,approvement stages can be added for production usage

For countinous deployment terraform can be used with GitOps way
Currently developer need to terraform apply for every change on docker image.
Nobody wants to apply terraform for every tiny change :)

